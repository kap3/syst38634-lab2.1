package phones;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmartPhoneTest {

	@Test
    void testGetFormattedPriceRegular() {
        SmartPhone sp = new SmartPhone("iPhone", 1000, 10.0);
        String formattedPrice = sp.getFormattedPrice();
        assertTrue("The value provided does not match the result", formattedPrice.equals("$1,000"));
    }

    @Test 
    void testGetFormattedPriceException() {
        SmartPhone sp = new SmartPhone("iPhone", 1000, 10.0);
        String formattedPrice = sp.getFormattedPrice();
        assertTrue("The value provided does not match the result", formattedPrice.equals("$100"));
    }

    @Test
    void testGetFormattedPriceBI() {
        fail("Not yet implemented");
    }

    @Test
    void testGetFormattedPriceBO() {
        fail("Not yet implemented");
    }

    @Test
    void testSetVersionRegular(int version) throws VersionNumberException{

    }

    @Test
    void testSetVersionException(int version) throws VersionNumberException{

    }

    @Test
    void testSetVersionBI(int version) throws VersionNumberException{

    }

    @Test
    void testSetVersionBO(int version) throws VersionNumberException{

    }

}
